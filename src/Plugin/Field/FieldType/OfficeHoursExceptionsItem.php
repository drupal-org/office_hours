<?php

namespace Drupal\office_hours\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\office_hours\OfficeHoursDateHelper;

/**
 * Plugin implementation of the 'office_hours' field type.
 *
 * @FieldType(
 *   id = "office_hours_exceptions",
 *   label = @Translation("Office hours exception"),
 *   list_class = "\Drupal\office_hours\Plugin\Field\FieldType\OfficeHoursItemList",
 *   no_ui = TRUE,
 * )
 */
class OfficeHoursExceptionsItem extends OfficeHoursItem {

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    // @todo Add random Exception day value in past and in near future.
    $value = [];
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function formatTimeSlot(array $settings) {
    if ($this->day == OfficeHoursItem::EXCEPTION_DAY) {
      // Exceptions header does not have time slot.
      return '';
    }
    return parent::formatTimeSlot($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(array $settings) {
    $day = $this->day;
    if ($day === '' || $day === NULL) {
      // A new Exception slot.
      // @todo Value deteriorates in ExceptionsSlot::validate().
      $label = '';
    }
    elseif ($day == OfficeHoursItem::EXCEPTION_DAY) {
      $label = $settings['exceptions']['title'] ?? '';
    }
    else {
      $exceptions_day_format = $settings['exceptions']['date_format'] ?? NULL;
      $day_format = $settings['day_format'];
      $days_suffix = $settings['separator']['day_hours'];
      $pattern = $exceptions_day_format ? $exceptions_day_format : $day_format;

      if ($pattern == 'l') {
        // Convert date into weekday in widget.
        $label = \Drupal::service('date.formatter')->format($day, 'custom', $pattern);
      }
      else {
        $label = \Drupal::service('date.formatter')->format($day, $pattern);
        // Remove excessive time part.
        $label = str_replace(' - 00:00', '', $label);
      }
      $label .= $days_suffix;
    }

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function isExceptionDay() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isInRange($from, $to) {
    if ($to < $from) {
      // @todo Error. Raise try/catch exception.
      return FALSE;
    }

    if ($to < 0) {
      // @todo Undefined result. Raise try/catch exception.
      return FALSE;
    }

    if ($to == 0) {
      // All exceptions are OK.
      return TRUE;
    }

    $time = $this->parent->getRequestTime();

    $date = OfficeHoursDateHelper::format($to, 'Y-m-d');
    $lastday = (strtotime($date . ' +1 day'));

    $date = OfficeHoursDateHelper::format($from, 'Y-m-d');
    $yesterday = (strtotime($date . ' -1 day'));
    $today = (strtotime($date));

    $day = $this->day;
    // @todo Support not only ($from = today, $to = today).
    if (OfficeHoursDateHelper::isExceptionDay($to)) {
      // $from-$to are calendar dates.
      if ($day < $yesterday) {
        return FALSE;
      }
      elseif ($day == $yesterday) {
        // If the slot is until after midnight, it could be in range.
        return parent::isOpen($time);
      }
      elseif ($day <= $lastday) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    elseif ($from >= 0 && $from < OfficeHoursItem::SEASON_DAY) {
      // $from-$to is a range, e.g., 0..7 days.
      // Time slots from yesterday with endhours after midnight are included.
      $lastday = strtotime($date . " +$to day");
      if ($day >= $yesterday && $day <= $lastday) {
        return TRUE;
      }
      return FALSE;
    }
    else {
      // Undefined. $time is a real timestamp.
      return FALSE;
    }

  }

  public function isOpen($time)
  {
    $is_open = FALSE;

    $date = OfficeHoursDateHelper::format($time, 'Y-m-d');
    $yesterday = strtotime($date . ' -1 day');
    $today = strtotime($date);
    $day = $this->day;

    if ($day == $yesterday || $day == $today) {
      $is_open = parent::isOpen($time);
    }
    return $is_open;
  }

}
